#!/bin/bash
# https://github.com/jaor/xmobar/issues/239#issuecomment-233206552
# Dynamically change the xmobar width
#
# Run script from xmobar:
# `Run Com "/path/to/trayer.sh" [] "trayerpad" 10`

# Create transparent icon
create_xpm_icon () {
    timestamp=$(date)
    pixels=$(for i in `seq $1`; do echo -n "."; done)

    cat << EOF > "$2"
/* XPM *
static char * trayer_pad_xpm[] = {
/* This XPM icon is used for padding in xmobar to */
/* leave room for trayer-srg. It is dynamically   */
/* updated by by trayer-padding-icon.sh which is run  */
/* by xmobar.                                     */
/* Created: ${timestamp} */
/* <w/cols>  <h/rows>  <colors>  <chars per pixel> */
"$1 1 1 1",
/* Colors (none: transparent) */
". c none",
/* Pixels */
"$pixels"
};
EOF
}

# Width of the trayer window
width=$(xprop -name panel | grep 'program specified minimum size' | cut -d ' ' -f 5)

# Icon filename
iconfile="/tmp/trayer-${width}.xpm"

# Create a new icon if not exists
if [ ! -f $iconfile ]; then
    create_xpm_icon $width $iconfile
fi

# Output to xmobar
echo "<icon=${iconfile}/>"
