/*-*- mode: css; -*-*/
/*
 * Rofi color theme
 * Based on srcery color scheme for Vim
 * https://github.com/roosta/vim-srcery
 *
 * File: srcery.rasi
 * Desc: Srcery theme for Rofi
 * Author: Daniel Berg <mail@roosta.sh>
 * Modified: 2018-05-11
 */

* {
  spacing:       2;

  black:         #1C1B19;
  brightblack:   #918175;

  red:           #EF2F27;
  brightred:     #F75341;

  green:         #519F50;
  brightgreen:   #98BC37;

  yellow:        #FBB829;
  brightyellow:  #FED06E;

  blue:          #2C78BF;
  brightblue:    #68A8E4;

  magenta:       #E02C6D;
  brightmagenta: #FF5C8F;

  cyan:          #0AAEB3;
  brightcyan:    #53FDE9;

  white:         #D0BFA1;
  brightwhite:   #FCE8C3;

  orange:        #D75F00;
  brightorange:  #FF8700;

  xgray1:        #262626;
  xgray2:        #303030;
  xgray3:        #3A3A3A;
  xgray4:        #444444;
  xgray5:        #4E4E4E;

  highlight:     bold #FCE8C3;

  selected-normal-foreground:  @brightwhite;
  selected-normal-background:  @orange;

  normal-foreground:           @orange; // Text in prompt
  normal-background:           @black;  // Space between non-main entries

  alternate-normal-foreground: @brightblack;
  alternate-normal-background: @black;  // Space between non-main entries

  urgent-foreground:           @black;
  urgent-background:           @orange;

  selected-urgent-foreground:  @black;
  selected-urgent-background:  @red;

  alternate-urgent-background: @black;
  alternate-urgent-foreground: @red;

  active-foreground:           @yellow;
  active-background:           @xgray2;

  selected-active-foreground:  @black;
  selected-active-background:  @yellow;

  alternate-active-background: @yellow;
  alternate-active-foreground: @black;

  border-color:                @black;
  separatorcolor:              @white;
  background-color:            @black;  // Bar background
}


#window {
    anchor:     north;
    location:   north;
    width:      100%;
    border:     1px;
    children:   [ horibox ];
}

#horibox {
    orientation: horizontal;
    children:   [ prompt, entry, listview ];
}

#listview {
    layout:     horizontal;
    spacing:    2px;
    lines:      100;
}

#entry {
    expand:     false;
    width:      10em;
}

#element {
    padding:   0px 5px;
}

#element selected {
    background-color: @xgray1;
}

mainbox {
  border:  0;
  padding: 0;
}

message {
  border:  2px 0px 0px ;
  padding: 2px 0px 0px ;
}

textbox {
  highlight: @highlight;
  text-color: @brightwhite;
}

element-text normal.normal {
  background-color: @normal-background;
  text-color:       @normal-foreground;
}

element-text normal.urgent {
  background-color: @urgent-background;
  text-color:       @urgent-foreground;
}

element-text normal.active {
  background-color: @active-background;
  text-color:       @active-foreground;
}

element-text selected.normal {
  background-color: @selected-normal-background;
  text-color:       @selected-normal-foreground;
}

element-text selected.urgent {
  background-color: @selected-urgent-background;
  text-color:       @selected-urgent-foreground;
}

element-text selected.active {
  background-color: @selected-active-background;
  text-color:       @selected-active-foreground;
}

element-text alternate.normal {
  background-color: @alternate-normal-background;
  text-color:       @alternate-normal-foreground;
}

element-text alternate.urgent {
  background-color: @alternate-urgent-background;
  text-color:       @alternate-urgent-foreground;
}

element-text alternate.active {
  background-color: @alternate-active-background;
  text-color:       @alternate-active-foreground;
}

button {
  text-color: @brightwhite;
}

button selected {
  background-color: @magenta;
  text-color:       @brightwhite;
}

inputbar {
  spacing:    0px ;
  text-color: @normal-foreground;
  padding:    1px ;
  children:   [ prompt,textbox-prompt-colon,entry,case-indicator ];
}

case-indicator {
  spacing:    0;
  text-color: @normal-foreground;
}

entry {
  spacing:    0;
  text-color: @normal-foreground;
}

prompt {
  spacing:    0;
  text-color: @white;
}

textbox-prompt-colon {
  expand:     false;
  str:        ":";
  margin:     0px 0.3000em 0.0000em 0.0000em ;
  text-color: inherit;
}
