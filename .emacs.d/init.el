;; Default value is 800KB
;; Value is measured in bytes
(setq gc-cons-threshold (* 50 1000 1000))

(setq user-emacs-directory "~/.cache/emacs/")
(setq url-history-file (expand-file-name "url/history" user-emacs-directory)) ; URL history location

(setq make-backup-files nil)

;; Initialize package sources
(require 'package)

;; Setup repos
(setq package-archives '(("melpa"        . "https://melpa.org/packages/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")
                         ("gnu"          . "https://elpa.gnu.org/packages/")
                         ("org"          . "https://orgmode.org/elpa/")))

;; Load packages (now)
(package-initialize)
;; Prevent package loading another time (faster than first run bu useless)
(setq package-enable-at-startup nil)

;; Load 'use-package'
;; Useful for package configuration
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)

;; Always ensure all packages are installed
(setq use-package-always-ensure t)

;; https://genehack.blog/2020/04/a-bit-of-emacs-advice/
(defvar dmaxter/packages-refreshed nil
  "Flag for whether package lists have been refreshed yet")
(defun dmaxter/package-refresh (&rest args)
  "Refresh package metadata, if needed
   Ignores `args`"
  (unless (eq dmaxter/packages-refreshed t)
    (progn
      (package-refresh-contents)
      (setq dmaxter/packages-refreshed t))))
(advice-add 'package-install :before #'dmaxter/package-refresh)

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(global-set-key (kbd "C-M-u") 'universal-argument)

(defun dmaxter/evil-hook ()
  (dolist (mode '(custom-mode
                  eshell-mode
                  erc-mode
                  term-mode))
     (add-to-list 'evil-emacs-state-modes mode)))

(use-package evil
  :init
  (setq evil-want-integration t)         ; Integrate evil with other modules
  (setq evil-want-keybinding nil)        ; Evil keys in other modules
  (setq evil-want-C-u-scroll t)          ; Scroll buffer with C-u
  (setq evil-want-C-i-jump t)            ; C-i jump forward in the jump list
  (setq evil-respect-visual-line-mode t)
  :config
  (setq evil-search-module 'evil-search) ; Use Vim's search instead of Emacs'
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-shift-round nil)            ; Fix identation issues
  (add-hook 'evil-mode-hook 'dmaxter/evil-hook)
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)
  (define-key evil-insert-state-map (kbd "TAB") 'tab-to-tab-stop)

  ;; Set normal mode to certain buffers
  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package which-key
  :init
  (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-separator " ")
  (setq which-key-prefix-prefix "+") ; Key separator for combo keys
  (setq which-key-idle-delay 0.01))

(use-package general
  :config
  (general-evil-setup t)

  (general-create-definer dmaxter/leader-key
    :keymaps '(normal visual emacs)
    :prefix "SPC"
    :global-prefic "M-SPC"))

(setq inhibit-startup-message t)

(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)

(setq visual-bell t)

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ; Scroll one line at a time with mouse
(setq mouse-wheel-progressive-speed nil) ; Do not accelerate scrolling with mouse
(setq mouse-wheel-follow-mouse t) ; Scroll window under mouse
(setq scroll-step 1) ; Move 1 line at a time while scrolling with keyboard

(set-frame-parameter (selected-frame) 'alpha '(95 . 95))
(add-to-list 'default-frame-alist '(alpha . (95 . 95)))

(setq column-number-mode t)

(setq large-file-warning-threshold nil)

(setq vc-follow-symlinks t)

(setq show-trailing-whitespace t)

(use-package srcery-theme
  :ensure t
  :config
  (load-theme 'srcery t))

(setq display-time-format "%l:%M %p %b %y")
(setq display-time-default-load-average nil)

(use-package diminish)

(use-package super-save
  :defer t
  :diminish super-save-mode
  :config
  (super-save-mode +1)
  (setq super-save-auto-save-when-idle t)
  (setq auto-save-default nil)) ; Disable default auto-save

(global-auto-revert-mode 1)

(use-package paren
  :config
  (set-face-attribute 'show-paren-match-expression nil :background "blue")
  (show-paren-mode t)
  (setq show-paren-delay 0))

(setq-default tab-width 2)
(setq-default evil-shift-width tab-width)

(setq-default indent-tabs-mode nil)

; Indent whole buffer
(defun dmaxter/indent-buffer ()
  "Indent current buffer"
  (interactive)
  (indent-region (point-min) (point-max))
  (message "Buffer indented"))

; Indent region
(defun dmaxter/indent-region ()
  "Indent selected region"
  (interactive)
  (indent-region (region-beginning) (region-end))
  (message "Region indented"))

; Indent buffer-or-region
(defun dmaxter/indent-buffer-region ()
  "Indent region if selected, indent buffer otherwise"
  (interactive)
  (if (region-active-p)
      (dmaxter/indent-region)
    (dmaxter/indent-buffer)))

(dmaxter/leader-key
  "i" '(dmaxter/indent-buffer-region :which-key "Indent"))

(use-package evil-nerd-commenter
  :bind
  ("M-/" . evilnc-comment-or-uncomment-lines))

(use-package ws-butler
  :hook
  ((text-mode . ws-butler-mode)
   (prog-mode . ws-butler-mode)))

(use-package parinfer
  :hook
  ((clojure-mode . parinfer-mode)
   (common-lisp-mode . parinfer-mode)
   (emacs-lisp-mode . parinfer-mode)
   (scheme-mode . parinfer-mode)
   (lisp-mode . parinfer-mode))
   :config
   (setq parinfer-extensions '(defaults
                               pretty-parens
                               evil
                               smart-tab
                               smart-yank)))

(dmaxter/leader-key
  "t"  '(:ignore t :which-key "Toggles")
  "tp" 'parinfer-toggle-mode)

(use-package expand-region
  :bind
  (("M-e" . er/expand-region)))

(use-package undo-tree
  :config
  (global-undo-tree-mode)
  (evil-set-undo-system 'undo-tree))

(dmaxter/leader-key
  "u" '(undo-tree-visualize :which-key "Show undo tree"))

(dmaxter/leader-key
  "f"   '(:ignore t :which-key "Files")
  "fd"  '(:ignore t :which-key "Dotfiles")
  "fde" '((lambda () (interactive) (find-file "~/.emacs.d/README.org")) :which-key "Emacs")
  "fdt" '((lambda () (interactive) (find-file "~/.config/alacritty/alacritty.yml")) :which-key "Terminal")
  "fdw" '((lambda () (interactive) (find-file "~/.xmonad/README.org")) :which-key "Window Manager")
  "fn"  '((lambda () (interactive) (counsel-find-file "~/org/")) :which-key "Notes"))

(use-package ivy
  :diminish
  :bind
  (:map ivy-minibuffer-map
   ("TAB" . ivy-alt-done) ; Select with TAB key
   ("C-j" . ivy-next-line)
   ("C-k" . ivy-previous-line)
   :map ivy-switch-buffer-map  ; Only available when choosing buffer
   ("C-j" . ivy-next-line)
   ("C-k" . ivy-previous-line))
  :init
  (ivy-mode 1)
  :config
  (setq ivy-wrap t) ; Wrap around last and first candidates
  (setq ivy-count-format "(%d/%d) ")
  (setq enable-recursive-minibuffers t)

  ;; Use different regex strategies per completion command
  (push '(swiper      . ivy--regex-ignore-order) ivy-re-builders-alist)
  (push '(counsel-M-x . ivy--regex-ignore-order) ivy-re-builders-alist)

  ;; Set minibuffer height for different commands
  (setf (alist-get 'counsel-projectile-ag ivy-height-alist) 15)
  (setf (alist-get 'counsel-projectile-rg ivy-height-alist) 15)
  (setf (alist-get 'swiper ivy-height-alist) 15)
  (setf (alist-get 'counsel-switch-buffer ivy-height-alist) 7))

;; Better interface for Ivy and Counsel
(use-package ivy-rich
  :init
  (ivy-rich-mode 1)
  :config
  (setq ivy-format-function #'ivy-format-function-line))

(use-package counsel
  :bind
  (("M-x" . counsel-M-x)
   ("C-x b" . counsel-ibuffer)
   ("C-x C-f" . counsel-find-file))
  :config
  (setq ivy-initial-inputs-alist nil)) ; Don't start searches with ^

;; Improve sorting of fuzzy-match results
(use-package flx
  :defer t
  :init
  (setq ivy-flx-limit 10000))

(dmaxter/leader-key
  "SPC" '(counsel-M-x :which-key "Run module")
  "f"   '(:ignore t :which-key "Files")
  "ff"  '(counsel-find-file :which-key "Open file")
  "fj"  '(counsel-file-jump :which-key "Jump to file")
  "fr"  '(counsel-recentf :which-key "Open recent")
  "fR"  '(revert-buffer :which-key "Revert file"))

;; Kill all buffers and windows except the current
(defun dmaxter/kill-other-buffers ()
    "Kill all other buffers."
    (interactive)
    (mapc 'kill-buffer (buffer-list))
    (delete-other-windows))

(dmaxter/leader-key
  "b"  '(:ignore t :which-key "Buffers")
  "bb" '(ivy-switch-buffer :which-key "Buffer list")
  "bd" '(kill-current-buffer :which-key "Kill buffer")
  "bk" '(dmaxter/kill-other-buffers :which-key "Kill other buffers")
  "bn" '(next-buffer :which-key "Next buffer")
  "bp" '(previous-buffer :which-key "Previous buffer"))

(use-package default-text-scale
  :bind
  (("C-+"   . default-text-scale-increase)
   ("C--"   . default-text-scale-decrease)
   ("C-M-0" . default-text-scale-reset))
  :config
  (default-text-scale-mode))

(dmaxter/leader-key
  "w"  '(:ignore t :which-key "Windows")
  "wl" '(windmove-right :which-key "Move Right")
  "wh" '(windmove-left :which-key "Move Left")
  "wk" '(windmove-up :which-key "Move Up")
  "wK" '(delete-other-windows :which-key "Kill other windows")
  "wj" '(windmove-down :which-key "Move Bottom")
  "w/" '(split-window-right :which-key "Split Right")
  "w-" '(split-window-below :which-key "Split Below")
  "wx" '(delete-window :which-key "Kill Window")
  "ww" '(other-window :which-key "Switch Window")
  )

(use-package dired
  :ensure nil ;; To be able to configure emacs native package using use-package
  :commands (dired dired-jump)
  :config
  (setq dired-listing-switches "-alhD --group-directories-first") ; Get all files
  (setq dired-omit-files "^\\.[^.].*") ; Omit dotfiles
  (setq dired-omit-verbose nil) ; Show message when omitting files

  (autoload 'dired-omit-mode "dired-x") ; Use dired with extra features

  (add-hook 'dired-load-hook
            (lambda ()
              (interactive)
              (dired-collapse))) ; Use GitHub way of showing nested directories with only 1 directory inside
  (add-hook 'dired-mode-hook
            (lambda ()
              (interactive)
              (dired-omit-mode) ; Ommit files
              (hl-line-mode 1)))) ; Highlight current line

(use-package dired-single ; Reuse current buffer
  :defer t)

(use-package dired-collapse
  :defer t)

(evil-collection-define-key 'normal 'dired-mode-map
  "h" 'dired-single-up-directory
  "H" 'dired-omit-mode
  "l" 'dired-single-buffer)

;; Turn on indentation and auto-fill mode for .org files
(defun dmaxter/org-mode-setup ()
  (org-indent-mode)
  (auto-fill-mode 1)
  (visual-line-mode 1)
  (setq evil-auto-indent nil)
  (diminish org-indent-mode))

(use-package org
  :defer t
  :hook
  (org-mode . dmaxter/org-mode-setup)
  :config
  (setq org-hide-emphasis-markers t) ; Hide the markup characters

  ;; Set options for code blocks
  (setq org-src-fontify-natively t) ;;; Use highlight
  (setq org-edit-src-content-indentation 0)
  (setq org-hide-block-startup nil)
  (setq org-src-preserve-indentation nil) ; Preserve whitespaces on export

  (setq org-startup-folded t) ; Start with content folded
  (setq org-cycle-separator-lines 2) ; Number of lines between end of subtree and headline

  (setq org-refile-use-outline-path t)

  (evil-define-key '(normal insert visual) org-mode-map (kbd "C-j") 'org-next-visible-heading)
  (evil-define-key '(normal insert visual) org-mode-map (kbd "C-k") 'org-previous-visible-heading)

(defun dmaxter/org-babel-tangle-no-ask ()
  (let ((org-confirm-babel-evaluate nil))
    (org-babel-tangle)))

(add-hook 'org-mode-hook (lambda ()
                           (add-hook 'kill-buffer-hook #'dmaxter/org-babel-tangle-no-ask
                                     'run-at-end 'only-in-org-mode)))

(use-package org-superstar
  :after org
  :hook
  (org-mode . org-superstar-mode)
  :custom
  (org-superstar-todo-bullet-alist t)
  (org-superstar-special-todo-items t)
  (org-superstar-remove-leading-stars t))
(require 'org-indent)

(use-package evil-org
  :after org
  :hook
  ((org-mode . evil-org-mode)
   (org-agenda-mode . evil-org-mode)
   (evil-org-mode . (lambda ()
                      (evil-org-set-key-theme '(navigation
                                                todo
                                                insert
                                                textobjects
                                                additional)))))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(setq org-agenda-files '("~/org/"))

(setq org-todo-keywords
    '((sequence "TODO" "IN-PROGRESS" "WAITING" "|" "DONE" "CANCELED" "DELAYED")))

(setq org-enforce-todo-dependencies t) ;;; Mark parent as DONE only if ALL child tasks marked as DONE

(setq org-agenda-start-on-weekday 0) ;;; Start week on sunday

(setq org-agenda-window-setup 'only-window) ;;; Open org-agenda in full window

;; Function to skip over tasks with given priority
;; https://blog.aaronbieber.com/2016/09/24/an-agenda-for-life-with-org-mode.html
(defun dmaxter/org-skip-subtree-if-priority (priority)
 "Skip an agenda subtree if it has a priority of PRIORITY.

  PRIORITY may be one of the characters ?A, ?B, or ?C."
(let ((subtree-end (save-excursion (org-end-of-subtree t)))
      (pri-value (* 1000 (- org-lowest-priority priority)))
      (pri-current (org-get-priority (thing-at-point 'line t))))
  (if (= pri-value pri-current)
      subtree-end
    nil)))

(setq org-agenda-custom-commands
    '(("c" "Full agenda view"
        ((tags-todo "PRIORITY=\"A\"" ; Select only TODO items
            ((org-agenda-overriding-header "Tasks to do in the nearest future:")))
         (agenda ""((org-agenda-overriding-header "Current week:")))
         (alltodo ""
            ((org-agenda-skip-function
                '(or (dmaxter/org-skip-subtree-if-priority ?A)
                     (org-agenda-skip-if nil '(scheduled deadline))))
             (org-agenda-overriding-header "Unfinished tasks:"))))
        ((org-agenda-compact-blocks t)))))

(setq org-capture-templates
    '(("b" "Basic task format" entry (file "tasks.org") "* TODO %?")
      ("s" "Scheduled task format" entry (file "agenda.org" "* TODO %?
SCHEDULED: %t"))))

)

   (dmaxter/leader-key
     "o"  '(:ignore t :which-key "Org")
     "oa" '((lambda () (interactive) (org-agenda "" "c")) :which-key "Agenda")
     "oc" 'org-capture
     "od" 'org-deadline
     "os" 'org-schedule
)

(use-package org-mime
  :after org)

(use-package magit
:commands (magit-status magit-get-current-branch)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package evil-magit
  :after magit)

(dmaxter/leader-key
  "g"  '(:ignore t :which-key "Magit")
  "gb" 'magit-branch
  "gc" 'magit-branch-or-checkout
  "gd" 'magit-diff-unstaged
  "gD" 'magit-diff-staged
  "gf" 'magit-fetch
  "gF" 'magit-fetch-all
  "gl" 'magit-log
  "gp" 'magit-push-current
  "gP" 'magit-pull-branch
  "gr" 'magit-rebase
  "gs" 'magit-status)

(use-package forge
  :after magit)

(use-package magit-todos
  :defer t)

(use-package projectile
  :diminish projectile-mode
  :init
  (setq projectile-switch-project-action #'projectile-dired)
  :config
  (projectile-mode))

(use-package counsel-projectile
  :after projectile)

(dmaxter/leader-key
  "p"  '(:ignore t :which-key "Projectile")
  "pc" 'projectile-compile-project
  "pd" 'projectile-dired
  "pf" 'counsel-projectile-find-file
  "pF" 'counsel-projectile-rg
  "pk" 'projectile-kill-buffers
  "pp" 'counsel-projectile
  "ps" 'counsel-projectile-switch-project)

(use-package dap-mode
  :hook
  (lsp-mode . dap-mode)
  :config
  (dap-ui-mode 1)
  (dap-tooltip-mode 1)
  (dap-ui-mode 1))

(use-package ccls
  :after lsp
  :hook
  ((c-mode c++-mode objc-mode cuda-mode) .
   (lambda () (require 'ccls) (lsp))))

(use-package omnisharp
  :mode "\\.cs\\'")

(use-package dockerfile-mode
  :mode "Dockerfile'")

(use-package haskell-mode
  :mode "\\.hs\\'")

(use-package lsp-haskell
  :after lsp)

(use-package web-mode
  :mode "\\.\\(html?\\|ejs\\|tsx\\|jsx\\)\\'"
  :config
  (setq-default web-mode-code-indent-offset 2)
  (setq-default web-mode-markup-indent-offset 2)
  (setq-default web-mode-attribute-indent-offset 2))

;; Preview HTML
;; Start module with `httpd-start`
(use-package impatient-mode)

(use-package skewer-mode)

;; Java Unit tests framework
(use-package groovy-mode
  :mode "\\.groovy\\'")

(use-package lsp-java
  :after lsp)

(use-package kotlin-mode
  :mode "\\.kt\\'")

(use-package lispy
  :hook
  ((emacs-lisp-mode . lispy-mode)
   (scheme-mode . lispy-mode)))

(add-hook 'emacs-lisp-mode-hook #'flycheck-mode)

(dmaxter/leader-key
  "e"  '(:ignore t :which-key "Eval")
  "eb" '(eval-buffer :which-key "Eval Buffer"))

(dmaxter/leader-key
  :keymaps '(visual)
  "er" '(eval-region :which-key "Eval region"))

(use-package markdown-mode
  :mode "\\.md\\'")

(use-package php-mode
  :mode "\\.php\\'")

(use-package protobuf-mode
  :mode "\\.proto\\'")

(use-package rustic
  :init
  (setq rustic-lsp-server 'rust-analyzer) ; RLS has been discontinued
  :config
  (setq lsp-rust-clippy-preference "on"
        rustic-lsp-format t
        lsp-rust-analyzer-server-display-inlay-hints t
        lsp-rust-analyzer-inlay-hints-mode t
        lsp-rust-analyzer-display-parameter-hints t
        lsp-rust-analyzer-display-chaining-hints t
        lsp-rust-analyzer-inlay-chain-format " » %s "
        lsp-rust-analyzer-cargo-load-out-dirs-from-check t
        lsp-rust-analyzer-proc-macro-enable t
        lsp-rust-rustfmt-path "/usr/bin/rustfmt")
  (add-hook 'before-save-hook (lambda () (when (or (eq 'rustic-mode major-mode) (eq 'rust-mode major-mode))
                                     (lsp-format-buffer))))
)

(use-package cargo
  :defer t)

(use-package nvm
  :defer t)

(use-package typescript-mode
  :mode "\\.ts\\'"
  :config
  (setq typescript-indent-level 2))

(defun dmaxter/js-indentation ()
  (setq js-indent-level 2)
  (setq evil-shift-width js-indent-level)
  (setq lsp-eslint-auto-fix-on-save t)
  (setq-default tab-width 2))

(use-package js2-mode
  :mode "\\.jsx?\\'"
  :config
  ;; Use this mode for Node scripts
  (add-to-list 'magic-mode-alist '("#!/usr/bin/env node" . js2-mode))

  ;; Disable builtin syntax checking
  (setq js2-mode-show-strict-warnings nil)

  ;; Set up proper indentation in JS and JSON files
  (add-hook 'js2-mode-hook #'dmaxter/js-indentation)
  (add-hook 'json-mode-hook #'dmaxter/js-indentation))

(use-package prettier-js
  :hook
  ((js2-mode . prettier-js-mode)
   (typescript-mode . prettier-js-mode)))

(use-package yaml-mode
  :mode "\\.ya?ml\\'")

(use-package ivy-xref
  :init
  (if (< emacs-major-version 27)
    (setq xref-show-xrefs-function #'ivy-xref-show-xrefs)
    (setq xref-show-definitions-function #'ivy-xref-show-defs)))

(use-package lsp-mode
  :commands lsp
  :hook
  ((typescript-mode js2-mode web-mode) . lsp)
  :bind
  (:map lsp-mode-map
   ("TAB" . completion-at-point)))

(use-package lsp-ui
  :hook
  (lsp-mode . lsp-ui-mode))

(dmaxter/leader-key
  "l"  '(:ignore t :which-key "LSP")
  "ld" 'xref-find-definitions
  "le" 'lsp-ui-flycheck-list
  "ln" 'lsp-ui-find-next-reference
  "lp" 'lsp-ui-find-prev-reference
  "lr" 'xref-find-references
  "ls" 'counsel-imenu
  "lS" 'lsp-ui-sideline-mode
  "lx" 'lsp-execute-code-action)

(use-package company
 :bind
 (:map
  company-active-map ("TAB" . company-complete-selection))
 :custom
 (company-idle-delay 0)
 (company-minimum-prefix-length 1)
)

(use-package company-lsp
   :custom
   (company-lsp-cache-candidates 'auto)
   (company-lsp-async t)
   (company-lsp-enable-snippet t)
   (company-lsp-enable-recompletion t)
)

(use-package flycheck
  :hook
  (lsp-mode . flycheck-mode))

(use-package smartparens
  :hook
  (prog-mode . smartparens-mode))

(use-package rainbow-delimiters
  :config
  ; Set better colors
  (set-face-attribute 'rainbow-delimiters-depth-1-face nil :foreground "dark orange")
  (set-face-attribute 'rainbow-delimiters-depth-2-face nil :foreground "deep pink")
  (set-face-attribute 'rainbow-delimiters-depth-3-face nil :foreground "chartreuse")
  (set-face-attribute 'rainbow-delimiters-depth-4-face nil :foreground "deep sky blue")
  (set-face-attribute 'rainbow-delimiters-depth-5-face nil :foreground "yellow")
  (set-face-attribute 'rainbow-delimiters-depth-6-face nil :foreground "orchid")
  (set-face-attribute 'rainbow-delimiters-depth-7-face nil :foreground "spring green")
  (set-face-attribute 'rainbow-delimiters-depth-8-face nil :foreground "sienna1")
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package rainbow-mode
  :hook
  (org-mode
   emacs-lisp-mode
   web-mode
   typescript-mode
   js2-mode))

; Line numbers
  (use-package nlinum)

  ;; Relative numbers
  (use-package nlinum-relative
    :ensure t
    :config
    (nlinum-relative-setup-evil)
    (setq nlinum-highlight-current-line t)
    (setq nlinum-format "%d")
    (setq nlinum-relative-redisplay-delay 0.01) ; Delay to refresh
(dolist (mode '(text-mode-hook
                prog-mode-hook
                conf-mode-hook))
  (add-hook mode #'nlinum-relative-mode)))

(use-package org-mu4e
  :disabled
  :config
  ;; Convert org mode to HTML automatically
  (setq org-mu4e-convert-to-html t))

(use-package mu4e-alert
  :after mu4e
  :hook
  ((after-init . mu4e-alert-enable-mode-line-display)
   (after-init . mu4e-alert-enable-notifications))
  :config
  (mu4e-alert-set-default-style 'libnotify))

(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e/")

(use-package smtpmail)

(use-package mu4e
  :ensure nil
  :config
  (setq mu4e-split-view 'horizontal)
  (setq mu4e-index-cleanup nil)     ;; don't do a full cleanup check
  ;(setq mu4e-index-lazy-check t)    ;; don't consider up-to-date dirs

(setq mu4e-maildir (expand-file-name (concat user-emacs-directory "/mu4e/mail")))
(setq mu4e-get-mail-command "mbsync -c ~/.emacs.d/mu4e/.mbsyncrc -a"
      ;; mu4e-html2text-command "w3m -T text/html" ;;using the default mu4e-shr2text
      mu4e-view-prefer-html nil
      mu4e-headers-auto-update t
      mu4e-compose-signature-auto-include nil
      mu4e-compose-format-flowed t)

(add-to-list 'mu4e-view-actions
             '("ViewInBrowser" . mu4e-action-view-in-browser) t)

;; Enable inline images
(setq mu4e-view-show-images t)

;; Use imagemagick, if available
(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))
;; Every new email composition gets its own frame!
(setq mu4e-compose-in-new-frame t)

;; Enable visual line mode when viewing
(add-hook 'mu4e-view-mode-hook #'visual-line-mode)

(add-hook 'mu4e-view-mode-hook
          (lambda()
            (interactive)
            (local-set-key (kbd "<RET>") 'mu4e~view-browse-url-from-binding)
            (local-set-key (kbd "<tab>") 'shr-next-link)
            (local-set-key (kbd "<backtab>") 'shr-previous-link)
            (setq-local show-trailing-whitespace nil)))

(add-hook 'mu4e-headers-mode-hook
          (defun mu4e-headers-setup()
	          (interactive)
            (setq-local show-trailing-whitespace nil)
	          (setq mu4e-headers-fields
	                `((:human-date . 25)
		                (:flags . 6)
		                (:from . 22)
		                (:thread-subject . ,(- (window-body-width) 70)) ;; alternatively, use :subject
		                (:size . 7)))))

(add-hook 'mu4e-compose-mode-hook
          (defun mu4e-compose-setup()
            (visual-line-mode)
            (org-mu4e-compose-org-mode)
            (use-hard-newlines -1)
            (flyspell-mode)))

;; Rename files when moving
(setq mu4e-change-filenames-when-moving t)

;; Queue for offline email
(setq smtpmail-queue-mail nil)

;; Set attachments dir
(setq mu4e-attachment-dir  "~/Downloads/.mu4e")

(setq message-kill-buffer-on-exit t)
(setq mu4e-compose-dont-reply-to-self t)

;; Show full addresses in view message (instead of just names)
(setq mu4e-view-show-addresses 't)

;; Don't ask when quitting
(setq mu4e-confirm-quit nil)

(setq mu4e-context-policy 'pick-first)
(setq mu4e-compose-context-policy 'always-ask)
(setq mu4e-contexts
      (list

(make-mu4e-context
 :name "gmail"
 :enter-func (lambda () (mu4e-message "Entering context Gmail"))
 :leave-func (lambda () (mu4e-message "Leaving context Gmail"))
 :match-func (lambda (msg)
		       (when msg
		         (mu4e-message-contact-field-matches
		          msg '(:from :to :cc :bcc) "daniel99matos@gmail.com")))
 :vars `((user-mail-address . "daniel99matos@gmail.com")
	       (user-full-name . "Daniel Matos")
	       (mu4e-sent-folder . "/gmail/Sent Mail")
	       (mu4e-drafts-folder . "/gmail/Drafts")
	       (mu4e-trash-folder . "/gmail/Junk")
	       (mu4e-compose-format-flowed . t)
         (smtpmail-queue-dir . ,(expand-file-name "mu4e/mail/gmail/queue/cur"   user-emacs-directory))
	       (message-send-mail-function . smtpmail-send-it)
	       (smtpmail-smtp-user . "daniel99matos")
	       (smtpmail-starttls-credentials . (("smtp.gmail.com" 587 nil nil)))
	       (smtpmail-default-smtp-server . "smtp.gmail.com")
	       (smtpmail-smtp-server . "smtp.gmail.com")
	       (smtpmail-smtp-service . 587)
	       (smtpmail-debug-info . t)
	       (smtpmail-debug-verbose . t)
	       (mu4e-maildir-shortcuts . ( ("/gmail/Inbox"     . ?i)
					                           ("/gmail/Sent Mail" . ?s)
					                           ("/gmail/Junk"      . ?t)
					                           ("/gmail/All Mail"  . ?a)
					                           ("/gmail/Starred"   . ?r)
					                           ("/gmail/Drafts"    . ?d)
                                     ("/gmail/SPAM"      . ?j)))))

(make-mu4e-context
  :name "tecnico"
  :enter-func (lambda () (mu4e-message "Entering context Tecnico"))
  :leave-func (lambda () (mu4e-message "Leaving context Tecnico"))
  :match-func (lambda (msg)
		        (when msg
		          (mu4e-message-contact-field-matches
		           msg '(:from :to :cc :bcc) "daniel.m.matos@tecnico.ulisboa.pt")))
  :vars `((user-mail-address . "daniel.m.matos@tecnico.ulisboa.pt")
	        (user-full-name . "Daniel Matos")
	        (mu4e-sent-folder . "/tecnico/Sent Mail")
	        (mu4e-drafts-folder . "/tecnico/Drafts")
	        (mu4e-trash-folder . "/tecnico/Junk")
	        (mu4e-compose-format-flowed . t)
          (smtpmail-queue-dir . ,(expand-file-name "mu4e/mail/tecnico/queue/cur"   user-emacs-directory))
	        (message-send-mail-function . smtpmail-send-it)
	        (smtpmail-smtp-user . "ist189429")
	        (smtpmail-default-smtp-server . "mail.tecnico.ulisboa.pt")
	        (smtpmail-smtp-server . "mail.tecnico.ulisboa.pt")
	        (smtpmail-smtp-service . 465)
          (smtpmail-stream-type . ssl)
	        (smtpmail-debug-info . t)
	        (smtpmail-debug-verbose . t)
	        (mu4e-maildir-shortcuts . ( ("/tecnico/Inbox"     . ?i)
					                            ("/tecnico/Sent Mail" . ?s)
                                      ("/tecnico/SO 2122"   . ?a)
					                            ("/tecnico/Junk"      . ?t)
					                            ("/tecnico/Drafts"    . ?d)
                                      ("/tecnico/SPAM"      . ?j)))))

(make-mu4e-context
          :name "outlook"
          :enter-func (lambda () (mu4e-message "Entering context Outlook"))
          :leave-func (lambda () (mu4e-message "Leaving context Outlook"))
          :match-func (lambda (msg)
		                (when msg
		                  (mu4e-message-contact-field-matches
		                   msg '(:from :to :cc :bcc) "daniel_matos@outlook.pt")))
          :vars `((user-mail-address . "daniel_matos@outlook.pt")
	                (user-full-name . "Daniel Matos")
	                (mu4e-sent-folder . "/outlook/Sent Mail")
	                (mu4e-drafts-folder . "/outlook/Drafts")
	                (mu4e-trash-folder . "/outlook/Junk")
	                (mu4e-compose-format-flowed . t)
                  (smtpmail-queue-dir . ,(expand-file-name "/mu4e/mail/outlook/queue/cur" user-emacs-directory))
	                (message-send-mail-function . smtpmail-send-it)
	                (smtpmail-smtp-user . "daniel_matos@outlook.pt")
	                (smtpmail-starttls-credentials . (("smtp-mail.outlook.com" 587 nil nil)))
	                (smtpmail-default-smtp-server . "smtp-mail.outlook.com")
	                (smtpmail-smtp-server . "smtp-mail.outlook.com")
	                (smtpmail-smtp-service . 587)
	                (smtpmail-debug-info . t)
	                (smtpmail-debug-verbose . t)
	                (mu4e-maildir-shortcuts . ( ("/outlook/Inbox"     . ?i)
					                                    ("/outlook/Sent Mail" . ?s)
					                                    ("/outlook/Junk"      . ?t)
					                                    ("/outlook/Drafts"    . ?d)
                                              ("/outlook/SPAM"      . ?j))))))))

;; Run every 15min
(run-at-time nil 900 'mu4e-update-mail-and-index 't)

(dmaxter/leader-key
  "m" '(mu4e :which-key "Email"))

(setq doc-view-continuous t)

(defun with-face (str &rest face-plist)
  (propertize str 'face face-plist))

(defun dmaxter/eshell-prompt ()
  (concat
   (with-face user-login-name :foreground "orange")
   "@"
   (propertize (system-name) 'face `(:foreground "white"))
   " "
   (with-face (if (string= (eshell/pwd) (getenv "HOME"))
     "~"
     (eshell/basename (eshell/pwd)))) ; Get only current folder from path

   ;; Check if version controlled
   (or (ignore-errors (format " (%s)" (vc-responsible-backend default-directory))) "")
   (if (= (user-uid) 0) ;;; Check if root
       (with-face " # " :foreground "red")
     " $ ")))

(setq eshell-prompt-function 'dmaxter/eshell-prompt) ;;; Set custom function for prompt creation
(setq eshell-highlight-prompt nil) ;;; Remove defaullt highlight
(add-hook 'eshell-mode-hook
          (lambda ()
            (setq show-trailing-whitespace nil) ;;; Remove trailing whitespaces
            ))

(use-package eshell-toggle
  :custom
  (eshell-toggle-use-projectile-root t)
  (eshell-toggle-run-command nil))

(dmaxter/leader-key
  "s" '(eshell-toggle :which-key "Shell"))

(setq ediff-diff-options "-w" ; Ignore newline
      ediff-window-setup-function 'ediff-setup-windows-plain)

(setq ediff-split-window-function 'split-window-horizontally)

(setq gc-cons-threshold (* 800 1000))
