# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

KEYTIMEOUT=1

# ***** Theme configuration *****
ZSH_THEME="../../.config/powerlevel10k/powerlevel10k"

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon user ssh dir_writable dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator vi_mode background_jobs time)

# CASE_SENSITIVE="true"
# HYPHEN_INSENSITIVE="true"

# DISABLE_AUTO_UPDATE="true"
# export UPDATE_ZSH_DAYS=13

# DISABLE_LS_COLORS="true"

ENABLE_CORRECTION="true"

# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

HIST_STAMPS="dd/mm/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git)

source $ZSH/oh-my-zsh.sh


# ***** User configuration *****

# Avoid duplicate commands in history
HISTCONTROL=ignoreboth

# Set different history for different tmux panes
if [[ $TMUX_PANE ]]; then
	HISTFILE=$HOME/histfiles/.zsh_history_tmux_$(tmux display-message -p '#S_#P')
else
	HISTFILE=$HOME/.zsh_history
fi

# Set vi keys
bindkey -v

# ***** Aliases *****

# Set colors for common commands
alias ls="exa"
alias cat="bat"

# Set checks for dangerous commands
alias cp="nocorrect cp -i"
alias mv="nocorrect mv -i"
alias rm="rm -i"

# CD to the location pointed by symlink
alias cd="cd -P"

# Dotfiles repo
alias dotgit="/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME"

# Python debugger
alias pdb="python3 -m pdb"

# ***** GNOME Keyring *****
if [ -n "$DESKTOP_SESSION" ]; then
	dbus-update-activation-environment --systemd DISPLAY
fi

source /usr/share/nvm/init-nvm.sh

# ***** ZSH Auto configuration *****

# The following lines were added by compinstall
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate rehash true
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTSIZE=1000
SAVEHIST=1000
# End of lines configured by zsh-newuser-install

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
