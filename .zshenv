# Add Ruby bin files path
export PATH=$PATH:/home/daniel/.gem/ruby/2.7.0/bin/:/home/daniel/.gem/ruby/3.0.0/bin:"/media/Dados/IST/MEIC/2ºAno/Tese/wasmati/bin":"/media/Dados/IST/MEIC/2ºAno/Tese/wasp/bin"

# Support for 256 colors
export TERM="xterm-256color"

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set XDG_CONFIG_HOME
export XDG_CONFIG_HOME="$HOME/.config"

# Set XDG_CACHE_HOME
export XDG_CACHE_HOME="$HOME/.cache"

# Set the JAVA_HOME (directory is managed by archlinux-java)
export JAVA_HOME=/usr/lib/jvm/default

# Java windows display
export _JAVA_AWT_WM_NONREPARENTING=1

# For tuxguitar
export LD_LIBRARY_PATH=/usr/lib/jvm/java-20-openjdk/lib/server

# Disable less history file
export LESSHISTFILE=-

# Set manpager
export MANPAGER="bat -l man -p"

export EDITOR="vim"
export VISUAL="vim"

export SSH_KEY_PATH="~/.ssh/rsa_id"

# Unlock keyring
if [ -n "$DESKTOP_SESSION" ];then
    eval $(gnome-keyring-daemon --start 2>/dev/null)
    export SSH_AUTH_SOCK
fi

# Add cargo binaries
export PATH=$PATH:~/.cargo/bin
