-- System
import XMonad
import System.Exit
import System.IO (hPutStrLn)

-- Data
import Data.Monoid
import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks (avoidStruts, docks, manageDocks)
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

-- Utilities
import Graphics.X11.ExtraTypes.XF86
import XMonad.Util.Run
import XMonad.Util.SpawnOnce

-- Layouts
import XMonad.Layout.Fullscreen
import XMonad.Layout.Grid
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.Tabbed

-- Layout Modifiers
import XMonad.Actions.NoBorders
import XMonad.Layout.LayoutModifier
import XMonad.Layout.NoBorders
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts

-- Monitor
import XMonad.Actions.CycleWS

-- Default applications
terminal      = "alacritty"
editor        = "emacs"
editorClient  = "emacsclient"
fileManager   = "ranger"
browser       = "firefox"

focusFollowsMouse  :: Bool
clickFocus         :: Bool

focusFollowsMouse  = True
clickFocus         = False

windowSpacing      = 5

-- Borders
borderWidth        = 2
normalBorderColor  = "#b2b2b2"
focusedBorderColor = "#FF5F00"

-- Tabs
tabActiveColor         = "#1c1b19"
tabActiveTextColor     = Main.focusedBorderColor
tabActiveBorderColor   = tabActiveTextColor
tabInactiveColor       = "#585858"
tabInactiveTextColor   = "#fce8c3"
tabInactiveBorderColor = tabInactiveTextColor
tabFontName            = "xft:Hack Nerd Font Mono:size=9:hinting=true:antialias=true"

-- Fading
--fadeAmount = 0.95
fadeAmount = 1

modKey = mod4Mask

keyBindings conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
  [
  -- Applications
    ((modm,               xK_Return), spawn $ XMonad.terminal conf)
  , ((modm,               xK_space ), spawn "rofi -show run")
  , ((modm,               xK_e     ), spawn (editorClient ++ " -c"))
  , ((modm .|. shiftMask, xK_e     ), spawn Main.editor)
  , ((modm,               xK_r     ), spawn (Main.terminal ++ " -e " ++ fileManager))
  , ((modm,               xK_b     ), spawn browser)
  , ((modm,               xK_o     ), spawn (editorClient ++ " -c $HOME/org"))

  -- Volume
  , ((0, xF86XK_AudioRaiseVolume), spawn "$HOME/.config/scripts/volume up")
  , ((0, xF86XK_AudioLowerVolume), spawn "$HOME/.config/scripts/volume down")
  , ((0, xF86XK_AudioMute       ), spawn "$HOME/.config/scripts/volume mute")

  -- Brightness
  , ((0, xF86XK_MonBrightnessUp  ), spawn "$HOME/.config/scripts/brightness up")
  , ((0, xF86XK_MonBrightnessDown), spawn "$HOME/.config/scripts/brightness down")

  -- Printscreen
  , ((0       , xK_Print), spawn "flameshot full -p $HOME/Screenshots")
  , ((mod1Mask, xK_Print), spawn "flameshot gui")

  -- Camera
  , ((0, xF86XK_WebCam), spawn "$HOME/.config/scripts/notificationToggle camera")

  -- Audio Controls
  , ((mod1Mask .|. shiftMask, xK_p            ), spawn "playerctl --player=spotify play-pause")
  , ((mod1Mask .|. shiftMask, xK_Left         ), spawn "playerctl --player=spotify previous")
  , ((mod1Mask .|. shiftMask, xK_Right        ), spawn "playerctl --player=spotify next")
  , ((0                     , xF86XK_AudioPlay), spawn "playerctl play-pause")

  -- Lock
  , ((modm, xK_n), spawn "loginctl lock-session")

  -- Change graphics card
  , ((modm, xK_F7), spawn "$HOME/.config/scripts/graphics")

  -- Window Focus
  , ((modm, xK_k), windows W.focusUp)
  , ((modm, xK_j), windows W.focusDown)
  , ((modm, xK_m), windows W.focusMaster)

  -- Window Swap
  , ((modm .|. shiftMask, xK_m), windows W.swapMaster)
  , ((modm .|. shiftMask, xK_j), windows W.swapDown)
  , ((modm .|. shiftMask, xK_k), windows W.swapUp)

  -- Master Area size
  , ((modm, xK_h), sendMessage Shrink)
  , ((modm, xK_l), sendMessage Expand)

  -- Window Operations
  , ((modm,               xK_t), withFocused $ windows . W.sink)
  , ((mod1Mask,           xK_b), withFocused toggleBorder)
  , ((modm .|. shiftMask, xK_r), refresh)
  , ((modm,               xK_x), kill)

  -- Layouts
  , ((modm,               xK_p    ), sendMessage NextLayout)
  , ((modm .|. shiftMask, xK_space), setLayout $ XMonad.layoutHook conf)

  -- Master Area Window Number
  , ((modm, xK_comma ), sendMessage (IncMasterN 1))
  , ((modm, xK_period), sendMessage (IncMasterN (-1)))

  -- Monitor
  , ((modm .|. shiftMask, xK_i), prevScreen)
  , ((modm, xK_i              ), nextScreen)

  -- XMonad Operations
  , ((modm              , xK_q), spawn "xmonad --recompile && xmonad --restart")
  , ((modm .|. shiftMask, xK_q), io (exitWith ExitSuccess))
  , ((modm .|. shiftMask, xK_h), spawn ("echo \"" ++ help ++ "\" | xmessage -file -"))
  ]
  ++
  -- Change Workspace
  [((m .|. modm, k), windows $ f i)
  | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
  , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]

mouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
  -- Move window
  [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster))
  -- mod-button2, Raise the window to the top of the stack
  , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
  -- Resize window
  , ((modm, button3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))
  ]

spacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
spacing i = spacingRaw False (Border windowSpacing i windowSpacing i) True (Border i windowSpacing i windowSpacing) True

grid = -- addTabs shrinkText tabTheme
        subLayout [] (Simplest)
       $ Main.spacing 0
       $ Grid

masterStack =  --addTabs shrinkText tabTheme
               subLayout [] (Simplest)
              $ Main.spacing 0
              $ Tall 1 (3/100) (1/2)

monocle =  --addTabs shrinkText tabTheme
           subLayout [] (Simplest)
          $ Full

tabs = tabbed shrinkText tabTheme

tabTheme = def { activeColor         = tabActiveColor
               , activeTextColor     = tabActiveTextColor
               , activeBorderColor   = tabActiveBorderColor
               , inactiveColor       = tabInactiveColor
               , inactiveTextColor   = tabInactiveTextColor
               , inactiveBorderColor = tabInactiveBorderColor
               , fontName            = tabFontName            }

layout = (avoidStruts $ smartBorders $ layouts) ||| Full
  where
    layouts = masterStack
              ||| tabs
              ||| grid
              ||| monocle
              ||| simplestFloat

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
    [ className =? "Gimp"           --> doFloat
    , className =? "vlc"            --> doFloat
    -- , className =? "Kodi"           --> doIgnore
    , className =? "MEGAsync"       --> doFloat
    , className =? "rambox"         --> doShift ( Main.workspaces !! 6 )
    , className =? ""               --> doShift ( Main.workspaces !! 7 ) ]
    <+> fullscreenManageHook

xmobarEscape :: String -> String
xmobarEscape = concatMap doubleList
  where
    doubleList '<' = "<<"
    doubleList x   = [x]

workspaces :: [String]
workspaces = clickable . (map xmobarEscape)
    $ ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
  where
    clickable l = [ "<action=xdotool key super+" ++ show(n) ++ ">" ++ ws ++ "</action>" |
                  (i, ws) <- zip [1..9] l,
                  let n = i]

logHook :: X ()
logHook = fadeInactiveLogHook fadeAmount

startup :: X ()
startup = do
  spawnOnce "trayer --edge top \
            \--align right \
            \--widthtype request \
            \--transparent true \
            \--alpha 0 \
            \--SetDockType true \
            \--SetPartialStrut true \
            \--expand true \
            \--tint 0x1c1b19 \
            \--height 21 &"                  -- System tray
  spawnOnce "nm-applet &"                    -- Network Manager
  spawnOnce "dunst &"                        -- Notifications
  spawnOnce "redshift &"                     -- Color temperature
  spawnOnce "nitrogen --restore &"           -- Wallpaper
  spawnOnce "picom &"                        -- Transparency
  spawnOnce "xsetroot -cursor_name left_ptr" -- Set appropriate cursor
  spawnOnce "rambox &"                       -- Message/Email client
  --spawnOnce "emacs --daemon &"               -- Emacs
  spawnOnce "spotify &"                      -- Spotify
  spawnOnce "kdeconnect-indicator &"         -- KDE Connect
  spawnOnce "setxkbmap pt"                   -- Keyboard mapping
  spawnOnce "unclutter &"                    -- Cursor auto-hide
  spawnOnce "rfkill block bluetooth"         -- Bluetooth block
  spawnOnce "megasync &"                     -- Cloud drive
  spawnOnce "syncthing -no-browser &"        -- Org files sync

  -- Set locker
  spawnOnce "xss-lock -n $HOME/.config/scripts/dimScreen -- $HOME/.config/scripts/lock &"

  -- Set power settings
  spawnOnce "xset s 87; xset -dpms"
  --setWMName "LG3D"  -- Fix WM bugs for not using DE

main = do
  xmproc <- spawnPipe "xmobar $HOME/.config/xmobar/xmobarrc" -- XMobar
  xmonad $ ewmh $ docks $def {         -- XMonad
    -- simple stuff
    XMonad.terminal           = Main.terminal,

    XMonad.focusFollowsMouse  = Main.focusFollowsMouse,
    clickJustFocuses          = clickFocus,

    XMonad.borderWidth        = Main.borderWidth,
    XMonad.normalBorderColor  = Main.normalBorderColor,
    XMonad.focusedBorderColor = Main.focusedBorderColor,
    XMonad.workspaces         = Main.workspaces,

    -- key bindings
    modMask                   = modKey,
    keys                      = keyBindings,
    XMonad.mouseBindings      = Main.mouseBindings,

    -- hooks, layouts
    layoutHook                = layout,
    manageHook                = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks,
    --handleEventHook           = docks,
    startupHook               = startup,
    XMonad.logHook            = workspaceHistoryHook <+> Main.logHook <+> dynamicLogWithPP xmobarPP
    -- XMobar Workspace config
    {
        ppOutput          = \x -> hPutStrLn xmproc x
      , ppCurrent         = xmobarColor "#2c78bf" "" . wrap "[" "]"  -- Current workspace
      , ppVisible         = xmobarColor "#fce8c3" ""                 -- Non-current workspaces
      , ppHidden          = xmobarColor "#fce8c3" "" . wrap "" ""    -- Hidden workspaces
      , ppHiddenNoWindows = \( _ ) -> ""                             -- Hide workspaces without windows
      , ppUrgent          = xmobarColor "#ef2f27" "" . wrap "*" "*"  -- Urgent workspaces
      , ppOrder           = \(ws:l:t:ex) -> [ws]                     -- Output
    }
  }

help :: String
help = unlines ["Mod key is Super. Default keybindings:",
  "",
  "-- launching and killing programs",
  "mod-Enter        Launch termite",
  "mod-Space        Launch dmenu",
  "mod-x            Close/kill the focused window",
  "mod-Space        Rotate through the available layout algorithms",
  "mod-Shift-Space  Reset the layouts on the current workSpace to default",
  "mod-n            Resize/refresh viewed windows to the correct size",
  "",
  "-- move focus up or down the window stack",
  "mod-Tab        Move focus to the next window",
  "mod-Shift-Tab  Move focus to the previous window",
  "mod-j          Move focus to the next window",
  "mod-k          Move focus to the previous window",
  "mod-m          Move focus to the master window",
  "",
  "-- modifying the window order",
  "mod-Return   Swap the focused window and the master window",
  "mod-Shift-j  Swap the focused window with the next window",
  "mod-Shift-k  Swap the focused window with the previous window",
  "",
  "-- resizing the master/slave ratio",
  "mod-h  Shrink the master area",
  "mod-l  Expand the master area",
  "",
  "-- floating layer support",
  "mod-t  Push window back into tiling; unfloat and re-tile it",
  "",
  "-- increase or decrease number of windows in the master area",
  "mod-comma  (mod-,)   Increment the number of windows in the master area",
  "mod-period (mod-.)   Deincrement the number of windows in the master area",
  "",
  "-- quit, or restart",
  "mod-Shift-q  Quit xmonad",
  "mod-q        Restart xmonad",
  "mod-[1..9]   Switch to workSpace N",
  "",
  "-- Workspaces & screens",
  "mod-Shift-[1..9]   Move client to workspace N",
  "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
  "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
  "",
  "-- Mouse bindings: default actions bound to mouse events",
  "mod-button1  Move window",
  "mod-button2  Move window to the top of the stack",
  "mod-button3  Set the window to floating mode and resize by dragging"]
