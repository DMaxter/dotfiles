execute pathogen#infect()

let g:srcery_bold = 1

let g:srcery_italic = 1

let g:srcery_undeline = 1

let g:srcery_undercurl = 1

let g:srcery_inverse_match_paren = 0

let g:srcery_transparent_background = 1

set termguicolors

colorscheme srcery

nnoremap gf :vertical wincmd f<CR>

nnoremap <silent> <C-k> :tabnew %<CR>

function! ToggleNetrw()
	let on = 0

	let i = bufnr("$")
	while (i >= 1)
		if (getbufvar(i, "&filetype") == "netrw")
			silent exe "bwipeout " . i
			let on = 1
		endif
		let i-=1
	endwhile

	if (on == 0)
  silent Vexplore | wincmd H | exec 'vertical resize '.string(&columns * 0.1)
  endif
endfunction

nnoremap <silent> <C-h> :call ToggleNetrw()<CR>

let g:netrw_liststyle = 3

let g:netrw_banner = 0

let g:netrw_altv = 1

let g:netrw_winsize = 15

let g:netrw_browse_split = 4

autocmd FileType netrw setl bufhidden=delete

let g:netrw_dirhistmax = 0

augroup VimStartup
	autocmd!
	autocmd VimEnter * if expand("%") == "" | :Vexplore | endif
augroup END

syntax on

set encoding=UTF-8

set number
set relativenumber

augroup numbertoggle
	autocmd!
	autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
	autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

set tabstop=4

set shiftwidth=4

set softtabstop=0
set expandtab

set incsearch

set hlsearch

set splitbelow

set splitright

set autoindent

set spelllang+=pt

set complete+=kspell

set wildmenu

autocmd VimResized * wincmd =

set autochdir

let g:airline_powerline_fonts = 1

let g:airline#extensions#tabline#enabled = 1

let g:airline#extensions#tabline#formatter = 'unique_tail'

let g:airline_theme = 'srcery'

let g:rainbow_active = 1

let g:ale_completion_enabled = 1

let g:CoolTotalMatches = 1

let delimitMate_expand_cr = 1

let delimitMate_expand_space = 1
